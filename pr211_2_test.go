package mergo

import (
	"reflect"
	"testing"
	"time"
)

type pr211Transformer struct {
}

func (s *pr211Transformer) Transformer(t reflect.Type) func(dst, src reflect.Value) error {
	return nil
}

func Test_deepMergeTransformerInvalidDestination(t *testing.T) {
	foo := time.Time{}
	src := reflect.ValueOf(foo)
	deepMerge(reflect.Value{}, src, make(map[uintptr]*visit), 0, &Config{
		Transformers: &pr211Transformer{},
	})
	// this test is intentionally not asserting on anything, it's sole
	// purpose to verify deepMerge doesn't panic when a transformer is
	// passed and the destination is invalid.
}
